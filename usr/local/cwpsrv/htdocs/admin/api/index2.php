<?php 
	function get_client_ip() {
	    $ipaddress = '';
	    if (getenv('HTTP_CLIENT_IP'))
	        $ipaddress = getenv('HTTP_CLIENT_IP');
	    else if(getenv('HTTP_X_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	    else if(getenv('HTTP_X_FORWARDED'))
	        $ipaddress = getenv('HTTP_X_FORWARDED');
	    else if(getenv('HTTP_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_FORWARDED_FOR');
	    else if(getenv('HTTP_FORWARDED'))
	       $ipaddress = getenv('HTTP_FORWARDED');
	    else if(getenv('REMOTE_ADDR'))
	        $ipaddress = getenv('REMOTE_ADDR');
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}

	$allowed_ip = trim(file_get_contents('/usr/local/cwp/.conf/api_allowed.conf'));
	$allowed_key = trim(file_get_contents('/usr/local/cwp/.conf/api_key.conf'));

	if ($allowed_ip == get_client_ip()) {
		if (isset($_GET['key'])) {
			if ($allowed_key == $_GET['key']) {
				$settings = parse_ini_file('/root/.my.cnf',true);
				$servername = "localhost";
				$username = $settings['client']['user'];
				$password = $settings['client']['password'];
				$dbname = "root_cwp";

				// Create connection
				$conn = new mysqli($servername, $username, $password, $dbname);
				// Check connection
				if ($conn->connect_error) {
				    die("Connection failed: " . $conn->connect_error);
				} 

				switch ($_GET['api']) {
					case 'package':
							$sql = "SELECT * FROM `packages` WHERE package_name='".$_GET['getName']."'";
							$result = $conn->query($sql);

							if ($result->num_rows > 0) {
							    while($row = $result->fetch_assoc()) {
							        $package_id = $row['id'];
							    }
							} else {
								$sql = "INSERT INTO `packages` (
								    `package_name`,
								    `disk_quota`,
								    `bandwidth`,
								    `ftp_accounts`,
								    `email_accounts`,
								    `email_lists`,
								    `databases`,
								    `sub_domains`,
								    `parked_domains`,
								    `addons_domains`,
								    `hourly_emails`
								) VALUES (
								    '".str_ireplace("%20", " ", $_GET['getName'])."',
								    '".$_GET['getQuota']."',
								    '".$_GET['getBandwidth']."',
								    '".$_GET['getMaxFtp']."',
								    '".$_GET['getMaxPop']."',
								    '".$_GET['getMaxPop']."',
								    '".$_GET['getMaxSql']."',
								    '".$_GET['getMaxSubdomains']."',
								    '".$_GET['getMaxParkedDomains']."',
								    '".$_GET['getMaxDomains']."',
								    '60'
								) ";
								if ($conn->query($sql)) {
								    $package_id = $conn->insert_id;
								} else {
								    savelog ("Error Exec $sql");
								}
							}

							echo $package_id;

						break;
					
					case 'changepass':
							$account = $_GET['account'];
							$password = $_GET['password'];
							shell_exec('echo "'.$account.':'.$password.'" | chpasswd');
						break;

					default:
						# code...
						break;
				}
			} else {
				echo 'Key not set or invalid!';
		}
		} else {
			echo 'Key not set or invalid!';
		}
	} else {
		echo "Allowed IP's not set or you don't have access from IP: ".get_client_ip()." !";
	}
